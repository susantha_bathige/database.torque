---
layout: post
title: 'SQLSaturday #908 - Denver'
date: '2019-10-15T13:24:00.001-07:00'
author: Susantha Bathige
tags: [meetup]
modified_time: '2019-10-15T13:24:52.797-07:00'
---

I completed another SQLSat session this week at Denver. It was pleasure to see about 20 folks for my presentation about SQL Server on Kubernetes. I’m really happy to see several sessions around containers which is really good. DBAs are not stepping into this new technology era of virtualization to run SQL Server or any other database workload on kuberneres.

[Twitter post](https://twitter.com/denversql/status/1183116130073894913?s=09){:target="_blank"}