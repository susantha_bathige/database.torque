---
layout: post
title: 'Couchbase: Bucket (Database) Architecture'
date: '2017-11-16T14:17:00.001-08:00'
author: Susantha Bathige
tags: [nosql,couchbase]
---

In Couchbase, a bucket is similar to a database in other DB systems. Application inserts data directly into a bucket(s) and there is no any other schema or objects inside it. A bucket contains documents.

Further reading, follow [this](https://www.linkedin.com/pulse/couchbase-bucket-database-architecture-susantha-bathige/" alt="https://www.linkedin.com/pulse/couchbase-bucket-database-architecture-susantha-bathige/){:target="_blank"} link.