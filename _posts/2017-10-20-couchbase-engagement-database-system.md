---
layout: post
title: Couchbase - the engagement database system
date: '2017-10-20T10:16:00.000-07:00'
author: Susantha Bathige
tags: [nosql,couchbase]
modified_time: '2017-10-20T16:09:39.498-07:00'
---
Today, I just wrapped up the four-day Couchbase administration course from Couchbase. Since I'm still new to this NoSQL world, it is tons of new stuff. Couchbase refers themselves as beyond just a NoSQL database, they call it engagement database system. 

Couchbase also has SQL style query language which they call it as N1QL. Using N1QL you can query the JSON documents stored in Couchbase buckets. 

What is so special about Couchbase with opposed to the other NoSQL databases like Cassandra and MongoDB is, they provide multi-dimensional scalability which is essentially, different database components like data service, query service, index service and full-text services can be scaled in/out independently. This concept makes distributed system even more complex in my opinion. The main advantage of multi-dimensional scaling is, you can select different hardware resources for each service to best suit its workload which is a very good thing. 

Couchbase provides very good free online courses if you want to get familiar with the database system.

[Couchbase Training](https://training.couchbase.com/){:target="_blank"}

So next few months I'll be working on Couchbase closely with automation on AWS platform. 
