---
layout: post
title: Monitoring SQL Server
date: 2020-08-18 11:25:00 -0600
description: Script I used to monitor SQL Server activities. 
img: aug-2020/torreys-peak.jpg
tags: [script, sqlserver]
---

Every SQL Server DBA has own set of scripts to monitor SQL Server. [This](https://gitlab.com/susantha.bathige/code/-/snippets/2005574){:target="_blank"} is the script I use at very first time if I need to see what is going on with any SQL Server instance. I have been using this with SQL Server 2016 to 2019. Script needs some tweaking for the prior versions of SQL Servers. 

Below is the same script for your quick reference. I commented out few lines which I uncomment only if I need to dig in more. 

<script src='https://gitlab.com/susantha.bathige/code/-/snippets/2005574.js'></script>

> <em>**About the post header picture**</em>: It was taken when hiking [Torreys peak](https://en.wikipedia.org/wiki/Torreys_Peak){:target="_blank"} mountain (14,275') on July, 2020. The peak you see at far away is the summit of Torreys peak. 
