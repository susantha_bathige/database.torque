---
layout: post
title: SQL Server query hints which are not much popular but very powerful
date: '2020-04-12T17:30:00.000-07:00'
author: Susantha Bathige
tags: [sqlserver, tsql]
img: posters/royal-arch-trail-3.jpg
modified_time: '2020-04-13T07:51:44.655-07:00'
---

I learned some new query hints which we can use when troubleshooting query performance issues. Those are mentioned below;

```sql
-- forces the query to use follow the join order as it specifies in the query and also makes all the joins as hash joins
option (force order, hash join) - 

-- to generate a query plan using the simple containment
option (use hint ('ASSUME_JOIN_PREDICATE_DEPENDS_ON_FILTERS'))

-- to eliminate the merge join
option ( hash join)

-- Force to use the legacy cardinality estimator
option ( force_Legacy_cardinality_estimation)

-- combine two query hints
option (use hint ('ASSUME_JOIN_PREDICATE_DEPENDS_ON_FILTERS')) and (force order )
```

## Further reading
* [Join containment assumption in the New Cardinality Estimator degrades query performance in SQL Server 2014 and later](https://support.microsoft.com/en-us/help/3189675/join-containment-assumption-in-the-new-cardinality-estimator-degrades){:target="_blank"}

* [Update introduces USE HINT query hint argument in SQL Server 2016](https://support.microsoft.com/en-us/help/3189813/update-introduces-use-hint-query-hint-argument-in-sql-server-2016){:target="_blank"}

> <em>**About the post header picture**</em>: It was taken during the hike in [Roral Arch Trail](https://www.alltrails.com/trail/us/colorado/royal-arch-trail){:target="_blank"} in Colorado Mar, 7th 2020. This is how the city is seen from the Arch Trail final point.