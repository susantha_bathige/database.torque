---
layout: post
title: Future versions of SQL Server?
date: '2012-11-25T09:40:00.001-08:00'
author: Susanthab
tags: 
modified_time: '2012-11-25T09:40:24.866-08:00'
blogger_id: tag:blogger.com,1999:blog-290532379135471177.post-2512669367745148606
permalink: /2012/11/future-versions-of-sql-server.html
---

<p align="justify">It is publicly known fact that RDBMSs have well known limitations when it comes to unstructured data management. This is the reason to emerged new database technologies like NoSQL. It is true that NoSQL is not a replacement for RDBMS. However the time has come to include NoSQL features into RDBMS products (then it can’t be called it as RDBMS) or introduce brand new NoSQL product from RDBMS vendors. We need to wait and see how Microsoft react to these new technology trends and how SQL Server change accordingly. Oracle has already announced their NoSQL version called <a href="http://www.oracle.com/us/products/database/nosql/overview/index.html" target="_blank">Oracle NoSQL Database</a>. Will Microsoft come up with new NoSQL product?</p>  