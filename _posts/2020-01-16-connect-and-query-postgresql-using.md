---
layout: post
title: Connect and query PostgreSQL using Azure Data Studio
date: '2020-01-16T14:59:00.000-08:00'
author: Susanthab
tags: [postgresql, tools]
modified_time: '2020-01-16T14:59:31.589-08:00'
---

Azure Data Studio is a new cross-platform database tool developed by Microsoft. It runs on Windows, macOS, and Linux. You can use the same tool to connect to PostgreSQL servers as well.

If you're new to this tool, you can download and install it. You need to install the Postgres extension for the Azure Data Studio to connect to PostgreSQL servers.

![connection details property]({{site.baseurl}}/assets/img/2020/PostgresConnectionProperty.jpg)

![connection details]({{site.baseurl}}/assets/img/2020/PostgresConnection.jpg)

Please note that if the PostgreSQL is the remote server then you need to make sure postgres is accepting the remote connection and also it allows your machine IP. You can change the listen_address property of the postgresql.conf file to '*' to allow remote connections and also you need to add your machine's IP address in pg_hba.conf file. When adding the IP address it has to be according to the CIDR notation, for example, 192.168.0.15/24.

If you do not perform the above config changes the errors below will be typical.
```
could not connect to server: Connection timed out (0x0000274C/10060)
 Is the server running on host "172.99.234.99" and accepting
 TCP/IP connections on port 5432?

FATAL:  no pg_hba.conf entry for host "10.52.99.99", user "postgres", database "postgres", SSL off
```

> References:
[What is Azure Data Studio](https://docs.microsoft.com/en-us/sql/azure-data-studio/what-is?view=sql-server-ver15){:target="_blank"}
[Quickstart: Connect and query PostgreSQL using Azure Data Studio](https://docs.microsoft.com/en-us/sql/azure-data-studio/quickstart-postgres?view=sql-server-ver15){:target="_blank"}

