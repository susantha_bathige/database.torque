---
layout: post
title: Database Sharding
date: '2011-10-11T06:05:00.000-07:00'
author: Susanthab
tags: 
modified_time: '2011-10-11T06:05:56.349-07:00'
blogger_id: tag:blogger.com,1999:blog-290532379135471177.post-3879879232697271921
permalink: /2011/10/database-sharding.html
---

Database sharding is a concept of dividing large databases to small data sets to scale out applications. Following article gives the in-out detail about sharding.<br /><br /><a href="http://www.codefutures.com/database-sharding/">http://www.codefutures.com/database-sharding/</a>