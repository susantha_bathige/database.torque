---
layout: post
title: A Co-Relational Model of Data for Large Shared Data Banks
date: '2011-04-11T04:39:00.000-07:00'
author: Susanthab
tags:
- NoSQL
modified_time: '2011-04-11T10:09:27.350-07:00'
blogger_id: tag:blogger.com,1999:blog-290532379135471177.post-1736040516003320108
permalink: /2011/04/co-relational-model-of-data-for-large.html
---

<span class="Apple-style-span" style="font-family: Verdana, sans-serif;">This is very&nbsp;interesting&nbsp;research paper about NoSQL and other database related matters.&nbsp;</span><br /><span class="Apple-style-span" style="font-family: Verdana, sans-serif;"><br /></span><br /><span class="Apple-style-span" style="font-family: Verdana, sans-serif;"><a href="http://queue.acm.org/detail.cfm?id=1961297">http://queue.acm.org/detail.cfm?id=1961297</a></span>