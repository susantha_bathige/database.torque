---
layout: post
title: Mount Additional Disks To CentOS/Linux
date: '2020-07-31T20:09:00.000-07:00'
author: Susanthab
tags: [linux]
modified_time: '2020-07-31T20:09:00.031-07:00'
img: aug-2020/grays-torrys-peak-trailheadbridge.jpg
permalink: /2020/07/mount-additional-disks-to-centoslinux.html
---

When you create CentOS VM with additional disks, you need to do additional steps to mount them to the file system. This blog post describes the steps on how to do it. First step is to identify the unmounted disks. You can use **lsblk -l** command as below::

![unmounted-disks]({{site.baseurl}}/assets/img/aug-2020/unmounted-disks.jpg)

You can see sdc device is not mounted to the file system. The MOUNTPOINT column is blank for sdc device. Let’s see how do we mount it to the file system.

First, you need to format the disk using mkfs command as below: The ext4 is the filesystem type.

![format-disk]({{site.baseurl}}/assets/img/aug-2020/format-disk.jpg)

The device name you have to give it as **/dev/sdc**. Formatting the disk is now complete and let’s mount the disk. I want to mount the disk as <em>pg_wal name</em>. This VM is going to host EDB Postgres instance and I want this 15 GB disk to keep the WAL files. 

Let’s create the mount point.
```console
$ sudo mkdir /pg_wal
```
The final step is to mount the disk. See below command. 
```console
$ sudo mount /dev/sdc /pg_wal
```
The new device is now successfully mounted. See below screenshot:

![mounted-disk]({{site.baseurl}}/assets/img/aug-2020/mounted-disk.jpg)

The OS I used in this example is CentOS 8.0. 

> <em>**About the post header picture**</em>: Trail head bridge at [Grays peak and Torreys peak](https://www.alltrails.com/trail/us/colorado/grays-and-torreys-peak){:target="_blank"} mountains on July, 2020.