---
layout: post
title: 'PostgreSQL: How to find the location of Postgres error log?'
date: '2020-05-14T14:05:00.000-07:00'
author: Susantha Bathige
tags: [postgresql]
img: posters/royal-arch-trail-2.jpg
modified_time: '2020-06-04T11:16:36.578-07:00'
---

I’m kind of new to Postgres. However, I’d like to work on Postgres as often as I could because that is how you can learn.
Recently, I had this opportunity to troubleshoot an issue in Postgres. I wanted to check the Postgres error log as a starting point. I spent some time to figure out where the error log is. This was in CentOS 7.0. I initially thought it would be in /var/log directory because it is the most common location of the error logs. The messages (CentOS) error log was in /var/log but nothing was related to Postgres logs in there.
After that, I did some Google search and found out you can get the Postgres configuration data from psql command prompt itself.

For example, if you want to check the Postgres config file location you can just type “SHOW CONFIG_FILE” in psql and it gives you the absolute path of the config file. See the below screenshot.

![config_file]({{site.baseurl}}/assets/img/may-2020/postgres_config_file.jpg)

Correspondingly, you’re able to check the configuration options specified in the conf file using the SHOW command.

In order to see the error log, you can use, “SHOW log_directory” command which gives you the log file location. See below screenshot;

![error_log]({{site.baseurl}}/assets/img/may-2020/postgres_error_log_file.jpg)

However, it did not give me the absolute path in this case rather I’d think it gave the relative path. Of course, these are the values you’ve given at the time of installation or afterward in Postgres and then they are stored in the config file. IMO, it is always a good habit to specify the absolute path wherever possible.

So the question remains the same, how do we find out the error log path?

Next, I checked the data directory location by using the command, SHOW data_directory and it gave me this;

![data_file]({{site.baseurl}}/assets/img/may-2020/postgres_data_file.jpg)

Good starting point. It is an absolute path. I thought, maybe, the “log” in the log_directory output could be the relative path to the data directory and with that in mind, I checked this path, /var/lib/pgsql/12/data/log. There you go. Finally, that is where the error logs are located in Postgres.

The Postgres version of this experiment was done against, PostgreSQL 12.1.

> <em>**About the post header picture**</em>: It was taken during the hike in [Roral Arch Trail](https://www.alltrails.com/trail/us/colorado/royal-arch-trail){:target="_blank"} in Colorado. The picture is shown, we hiking down the hill on Mar, 7th 2020. 